#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
lista = []
lista_final = []
inicio = 0
vecino_vivo = 0
i=0
tamano = int(input("Ingrese el tamaño de la lista: "))

def reglas(i, vecino_vivo, lista, lista_final):
#condiciones de las celulas 
	if((lista [i] == "[X]") and ((vecino_vivo > 1) or (vecino_vivo < 4))):
		lista_final.append("[X]")
		# ~ print ("La celula sobrevive")
	elif((lista [i] == "[X]") and (vecino_vivo >= 4)):
		lista_final.append("[-]")
		# ~ print ("La celula muere por sobrepoblacion")
	elif((lista [i] == "[X]") and (vecino_vivo <= 1)):
		lista_final.append("[-]")
		# ~ print ("La celula muere por soledad")
	elif((lista[i] == "[-]") and (vecino_vivo == 3)):
		lista_final.append("[X]")
		# ~ print ("La celula volvera a vivir")
	else:
		lista_final.append("[-]")
	return lista_final
	
	
def condiciones(i, inicio, tamano, lista, vecino_vivo):
	# ~ condiciones para el lado izquierdo
	if ((i == 0) or (i == inicio)):
		inicio = inicio + tamano
		if (lista[i] == "[X]"):
			print("VIVO")
			if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				print("ARRIBA")
			if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				print("ARRIBADERECHA")
			if(((i + 1) < (tamano * tamano)) and (lista[i + 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				print("DERECHA")
			if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				print("ABAJOM")
			if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				print("ABAJODERECHA")
		if(lista[i] == "[-]"):
			print("MUERTOS")
			if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				print("ARRIBA")
			if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				print("ARRIBADERECHA")
			if(((i + 1) < (tamano * tamano)) and (lista[i + 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				print("DERECHA")
			if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				print("ABAJO")
			if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				print("ABAJODERECHA")
	# ~ condiciones para el lado derecho
	elif (((i + 1) % tamano) == 0):
		if (lista[i] == "[X]"):
			# ~ print("VIVOS")
			if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ARRIBA")
			if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ABAJO")
			if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ARRIBAIZQ")
			if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("IZQ")
			if(((i + tamano - 1)  < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ABAJOIZQ")
		if(lista[i] == "[-]"):
			# ~ print("MUERTOS")
			if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ARRIBA")
			if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ABAJO")
			if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ARRIBAIZQ")
			if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("IZQ")
			if(((i + tamano - 1)  < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ABAJOIZQ")
	# ~ condiciones para las que estan al medio
	else:
		if (lista[i] == "[X]"):
			# ~ print("VIVA")
			if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ARRIBA")
			if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ARRIBADERECHA")
			if((i + 1) < (tamano * tamano)) and (lista[i + 1] == "[X]"):
				vecino_vivo = vecino_vivo + 1
				# ~ print("DERECHA")
			if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ABAJO")
			if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ABAJODERECHA")
			if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ABAJOIZQ")
			if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("IZQ")
			if(((i + tamano - 1)  < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ABAJOIZQ")
		if (lista[i] == "[-]"):
			# ~ print("MUERTA")
			if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ARRIBA")
			if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ARRIBADERECHA")
			if((i + 1) < (tamano * tamano)) and (lista[i + 1] == "[X]"):
				vecino_vivo = vecino_vivo + 1
				# ~ print("DERECHA")
			if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ABAJO")
			if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ABAJODERECHA")
			if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ARRIBAIZQ")
			if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("IZQ")
			if(((i + tamano - 1)  < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
				vecino_vivo = vecino_vivo + 1
				# ~ print("ABAJOIZQ")
	reglas(i, vecino_vivo, lista, lista_final)
	return inicio

for i in range(tamano * tamano):
	estado = random.randint(0, 1)
	if (estado == 0):
		lista.append("[X]")
	else:
		lista.append("[-]")
	print(lista[i], end = "")
	if ((i + 1) % tamano == 0):
		print("\n")		
# ~ condiciones
# ~ print(tamano)
i = 0 
while (i < (tamano * tamano)):
	condiciones(i, inicio, tamano, lista, vecino_vivo)
	i = i + 1 
	vecino_vivo = 0
	
print("\n")
for i in range(tamano * tamano):
	print (lista_final[i], end = "")
	if ((i + 1) % tamano == 0):
		print("\n")
